export const locale = {
    lang: 'en',
    data: {
        'NAV': {
            'APPLICATIONS': 'Applications',
            'SAMPLE'        : {
                'TITLE': 'Home',
                'BADGE': '25'
            },
            'DIAGRAM'        : {
                'TITLE': 'Diagram',
                'BADGE': '26'
            }
        }
    }
};
