export const locale = {
    lang: 'fr',
    data: {
        'NAV': {
            'APPLICATIONS': 'Applications',
            'SAMPLE'        : {
                'TITLE': 'Accueil',
                'BADGE': '25'
            },
            'DIAGRAM'        : {
                'TITLE': 'Diagramme',
                'BADGE': '26'
            }
        }
    }
};
