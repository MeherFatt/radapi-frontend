import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { UtilisateurDto } from '../../../common/dto/utilisateurDto';
import { Endpoints } from '../../../common/endpoints/endpoints';

@Injectable({providedIn: 'root'})
export class UserService {

   constructor(private readonly http: HttpClient,) {}

    getAll(): Observable<UtilisateurDto[]> {
        return this.http.get<UtilisateurDto[]>(Endpoints.ENDPOINT_UTILISATEURS);
    } 
}