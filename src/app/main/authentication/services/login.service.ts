import { Injectable } from '@angular/core';
import { UserService } from '../services/user.service';
import { UtilisateurDto } from '../../../common/dto/utilisateurDto';
import { AlerteService } from '../../../common/tools/alertes/alerte.service';

@Injectable({
  providedIn: 'root',
})
export class LoginService {
      
  utilisateurs: UtilisateurDto[];
  utilisateur: UtilisateurDto;

  constructor(private userService: UserService, private readonly _alerteService: AlerteService) {
    this.userService.getAll().subscribe(
      (users) => ( this.utilisateurs = users ),
      (error) => { this._alerteService.displayErrorMessage('Erreur de recuperation de donnees ...'); }
    );
  }

  login(email: string, password: string){  
    console.log('db users ---------> ', this.utilisateurs);
    return this.utilisateurs.find(
        (user) => user.email  === email && user.password === password
    );
    
  }
}
