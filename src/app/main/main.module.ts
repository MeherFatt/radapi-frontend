import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';
import { TranslateModule } from '@ngx-translate/core';

import { FuseSharedModule } from '@fuse/shared.module';

import { AccueilComponent } from './sample/accueil.component';
import { DiagramComponent } from './diagram/diagram.component';
import { DiagramDesignerModule } from 'app/diagram-designer/diagram-designer.module';

const routes = [
    {
        path     : 'accueil',
        component: AccueilComponent
    },
    {
        path      : 'diagram',
        component: DiagramComponent
    }
];

@NgModule({
    declarations: [
        AccueilComponent,
        DiagramComponent
    ],
    imports     : [
        RouterModule.forChild(routes),

        TranslateModule,

        FuseSharedModule,
        DiagramDesignerModule
    ]
})

export class MainModule
{
}
