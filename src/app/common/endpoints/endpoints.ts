
import { HttpHeaders } from '@angular/common/http';

export class Endpoints {

  constructor() { }

  static readonly HEADERS: HttpHeaders = new HttpHeaders().set('Content-Type', 'application/json');
  static readonly ROOT_API: string = 'https://api.myjson.com/bins';
  static readonly ENDPOINT_UTILISATEURS: string = Endpoints.ROOT_API + '/1gbqgy'; 
}
