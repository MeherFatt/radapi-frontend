
export class UtilisateurDto {
  id : number;
  nom: string;
  email: string;
  password: string;
  role: string;
  salaire: number;

  constructor() {}
}
