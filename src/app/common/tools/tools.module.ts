import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { AlertesComponent } from './alertes/alertes.component';



@NgModule({
  declarations: [
    AlertesComponent
  ],
  imports: [
    CommonModule
  ],
  exports: [
    AlertesComponent
  ]
})
export class ToolsModule { }
