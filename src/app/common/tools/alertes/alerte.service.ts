import { Injectable } from '@angular/core';
import { Router, NavigationStart } from '@angular/router';
import { Observable, Subject } from 'rxjs';
import { HttpErrorResponse } from '@angular/common/http';

@Injectable({ providedIn: 'root' })
/**
 * Permet de servir les messages à la volée en utilisant un Observable.
 */
export class AlerteService {
  private readonly subject = new Subject<any>();
  private keepAfterNavigationChange = false;

  constructor(private readonly router: Router) {
    // Le message reste ou pas avec la naviagation en faisant varier keepAfterNavigationChange
    router.events.subscribe((event) => {
      if (event instanceof NavigationStart) {
        if (this.keepAfterNavigationChange) {
          this.keepAfterNavigationChange = false;
        } else {
          this.subject.next();
        }
      }
    });
  }

  // Methode à utiliser pour envoyer le message depuis un component

  success(message: string, keepAfterNavigationChange = false) {
    this.keepAfterNavigationChange = keepAfterNavigationChange;
    this.subject.next({ type: 'success', text: message });
  }

  error(message: string, keepAfterNavigationChange = false) {
    this.keepAfterNavigationChange = keepAfterNavigationChange;
    this.subject.next({ type: 'error', text: message });
  }

  warning(message: string, keepAfterNavigationChange = false) {
    this.keepAfterNavigationChange = keepAfterNavigationChange;
    this.subject.next({ type: 'warning', text: message });
  }

  getMessage(): Observable<any> {
    return this.subject.asObservable();
  }


  displayErrorMessage(errorResponse: any, keepAfterNavigationChange = false) {

      this.keepAfterNavigationChange = keepAfterNavigationChange;
      this.error(JSON.stringify(errorResponse));
    
  }
}
