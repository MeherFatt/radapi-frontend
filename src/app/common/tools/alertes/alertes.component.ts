import { Component, OnInit, OnDestroy } from '@angular/core';
import { Subscription } from 'rxjs';
import { AlerteService } from './alerte.service';

@Component({
  selector: 'jasmine-alertes',
  templateUrl: 'alertes.component.html',
})
export class AlertesComponent implements OnInit, OnDestroy {
  private subscription: Subscription;
  messages = [];

  constructor(private readonly alertesService: AlerteService) {}

  ngOnInit() {
    this.subscription = this.alertesService.getMessage().subscribe((message) => {
      if (message) {
        this.messages.push(message);
        setTimeout(() => {
          this.messages = this.messages.filter((aMessage) => aMessage !== message);
        }, 5000);
      }
    });
  }

  ngOnDestroy() {
    this.subscription.unsubscribe();
  }
}
