
import { DiagramComponent, UmlClassifierShapeModel } from '@syncfusion/ej2-angular-diagrams';
import { NodeModel } from '@syncfusion/ej2-diagrams';

// Create class Diagram shapes.
export function createDefaulClasstNode(diagram: DiagramComponent, draggedNode: NodeModel) : NodeModel{
   const node = {
    id: 'id'+ Math.random(),
    shape: {
      type: 'UmlClassifier',
      classShape: {
        name: 'class name'+ Math.random(),
        attributes: [
          createProperty('Property', 'Type'),
          createProperty('Property', 'string[*]'),
        ],
        methods: [
          createMethods('method', 'bool'),
          createMethods('method', 'string')
        ]
      },
      classifier: 'Class'
    } as UmlClassifierShapeModel,
    offsetX: draggedNode.offsetX,
    offsetY: draggedNode.offsetY
  }
     diagram.addNode(node);
     return node;
  }


// create class Property
export function createProperty(name: string, type: string): object {
  return { name: name, type: type };
}

// create class Methods
export function createMethods(name: string, type: string): object {
  return { name: name, type: type };
}