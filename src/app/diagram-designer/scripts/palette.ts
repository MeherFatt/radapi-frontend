import { NodeModel, ConnectorModel } from '@syncfusion/ej2-diagrams';

// Palette Shapes
export function getBasicShapes(): NodeModel[] {

    let basicShapes: NodeModel[] = [
        { id: 'Rectangle', shape: { type: 'Basic', shape: 'Rectangle' }, style: { strokeWidth: 2 } },
        { id: 'Ellipse', shape: { type: 'Basic', shape: 'Ellipse' }, style: { strokeWidth: 2 } },
    ];
  
    return basicShapes;
  }
  
  // Platette Connectors  
  export function getConnectors(): ConnectorModel[] {
    let connectorSymbols: ConnectorModel[] = [
      // Association Directional
        {
          id: "connectorDirectional",
         //Define connector start and end points
         sourcePoint: { x: 100, y: 100 },
         targetPoint: { x: 300, y: 300 },
         type: "Straight",
         shape: {
           type: "UmlClassifier",
           relationship: "Association",
           //Define type of association
           association: "Directional"
         }
       },
     // Association BiDirectional
       {
        id: "connectorBiDirectional",
       //Define connector start and end points
       sourcePoint: { x: 100, y: 100 },
       targetPoint: { x: 300, y: 300 },
       type: "Straight",
       shape: {
         type: "UmlClassifier",
         relationship: "Association",
         //Define type of association
         association: "BiDirectional"
       }
     },
       
     // Aggregation
       {
        id: "connectorAggregation",
       //Define connector start and end points
       sourcePoint: { x: 100, y: 100 },
       targetPoint: { x: 300, y: 300 },
       type: "Straight",
       shape: {
        type: "UmlClassifier",
        //Set an relationship for connector
        relationship: "Aggregation"
       }
     },
     // Composition
     {
      id: "connectorComposition",
     //Define connector start and end points
     sourcePoint: { x: 100, y: 100 },
     targetPoint: { x: 300, y: 300 },
     type: "Straight",
     shape: {
       type: "UmlClassifier",
       //Set an relationship for connector
       relationship: "Composition"
     }
   },
   //Dependency
   {
    id: "connectorDependency",
   //Define connector start and end points
   sourcePoint: { x: 100, y: 100 },
   targetPoint: { x: 300, y: 300 },
   type: "Straight",
   shape: {
    type: "UmlClassifier",
     //Set relationship for connector
     relationship: "Dependency"
   }
  },
  // Inheritance
  {
    id: "connectorInheritance",
   //Define connector start and end points
   sourcePoint: { x: 100, y: 100 },
   targetPoint: { x: 300, y: 300 },
   type: "Straight",
   shape: {
    type: "UmlClassifier",
     //set an relation of connector
     relationship: "Inheritance"
   }
  }
    ];
    return connectorSymbols;
  };