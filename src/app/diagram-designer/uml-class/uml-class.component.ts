import { Component, OnInit, Input } from '@angular/core';
import { OnChanges, SimpleChanges, SimpleChange } from '@angular/core'
import { FormGroup, FormBuilder, Validators, FormControl } from '@angular/forms';
import { NodeModel, UmlClassifierShapeModel } from '@syncfusion/ej2-diagrams';

@Component({
  selector: 'app-uml-class',
  templateUrl: './uml-class.component.html',
  styleUrls: ['./uml-class.component.scss']
})
export class UmlClassComponent implements OnInit, OnChanges {

  @Input() nodeProperties: NodeModel;  
  angForm: FormGroup;
  shape: UmlClassifierShapeModel;

  constructor(private fb: FormBuilder) {}
  
  createForm() {  
    if(this.nodeProperties){
      this.shape = this.nodeProperties.shape as UmlClassifierShapeModel;
      this.angForm = this.fb.group({
        className: new FormControl({ value: this.shape.classShape.name, disabled: false }, Validators.required),
        properties: new FormControl({ value: this.shape.classShape.attributes[0], disabled: false }, Validators.required),
        methods: new FormControl({ value: this.shape.classShape.methods[0], disabled: false }, Validators.required)
      });
    }   
  }

  ngOnInit() {   
    this.createForm();
  }

  ngOnChanges() {
    this.createForm();
  }

}
