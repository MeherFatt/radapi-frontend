import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { DiagramModule, SymbolPaletteModule, DataBindingService } from '@syncfusion/ej2-angular-diagrams';
import { DiagramDesignerComponent } from './diagram-designer.component';
import { UmlClassComponent } from './uml-class/uml-class.component';

@NgModule({
  declarations: [
    DiagramDesignerComponent,
    UmlClassComponent
  ],
  imports: [
    CommonModule,
    FormsModule,
    ReactiveFormsModule,
    DiagramModule,
    SymbolPaletteModule,
  ],
  exports: [DiagramDesignerComponent] 
})
export class DiagramDesignerModule { }
