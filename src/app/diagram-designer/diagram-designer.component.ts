import { Component, OnInit, ViewEncapsulation, ViewChild } from '@angular/core';
import { DiagramComponent } from '@syncfusion/ej2-angular-diagrams';
import { Diagram, NodeModel, UndoRedo, IDragEnterEventArgs, MarginModel, PaletteModel, IDropEventArgs, BasicShapeModel, ConnectorModel, Connector, Node, UmlClassifierShapeModel  } from '@syncfusion/ej2-diagrams';
import { ExpandMode } from '@syncfusion/ej2-navigations';
import { createDefaulClasstNode, createProperty, createMethods } from './scripts/diagram';
import { getBasicShapes, getConnectors } from './scripts/palette';
import { getSavedDiagram } from './scripts/data';

Diagram.Inject(UndoRedo);

@Component({
  selector: 'app-diagram-designer',
  templateUrl: './diagram-designer.component.html',
  styleUrls: ['./diagram-designer.component.scss'],
  encapsulation: ViewEncapsulation.None
})
export class DiagramDesignerComponent implements OnInit{

@ViewChild('diagram', {static: false}) diagram: DiagramComponent;
node: NodeModel;
nodes: NodeModel[] = [];
selectedNode: NodeModel;
showPropertiesPanel: boolean = false;

//Loading Diagram
saveData: any;

constructor(){}

ngOnInit(): void
{
}
created(args: Object): void {}
nodeCollectionChange(args): void {}
dragEnter(args: IDragEnterEventArgs): void {}

// Diagram Properties
public serializationSettings: object = {  
//  preventDefaults: true  
}  

// Set the default values of nodes.
 getNodeDefaults(obj: NodeModel): NodeModel {
  obj.style = { fill: '#26A0DA', strokeColor: 'white' };
  return obj;
}

// Set the default values of connectors.
getConnectorDefaults(connector: ConnectorModel): ConnectorModel {
  connector.shape = {
    type: 'UmlClassifier',
    relationship: 'Association',
    multiplicity: {
        type: 'OneToOne'
    }
};
return connector;
}

// Set an annoation style at runtime.
setNodeTemplate(node: NodeModel): void {
  if (node.annotations.length > 0) {
    for (let i: number = 0; i < node.annotations.length; i++) {
      node.annotations[i].style.color = 'white';
    }
  }
}

//SymbolPalette Properties
symbolMargin: MarginModel = { left: 15, right: 15, top: 15, bottom: 15 };
expandMode: ExpandMode = 'Multiple';
palettes: PaletteModel[] = [
  {
    id: 'flow',
    expanded: false,
    symbols: getBasicShapes(),
    iconCss: 'shapes',
    title: 'Flow Shapes'
  },
  {
      id: 'connectors',
      expanded: true,
      symbols: getConnectors(),
      title: 'Connectors',
      iconCss: 'e-ddb-icons e-connector'
  }
];

// Changer le shape séléctionné de la palette et ..
// Si c'est un Rectangle, remove node et inserer node de type classe uml
dropEvent(args: IDropEventArgs): void {  

  let createdNode : NodeModel;
  const  draggedNode = args.element as NodeModel;
  const draggedNodeShape = draggedNode.shape as BasicShapeModel ;
  
  if(draggedNodeShape.shape === 'Rectangle'){
    args.cancel = true; 
     createdNode = createDefaulClasstNode(this.diagram, draggedNode);
    }

    this.selectedNode = createdNode;
    this.showPropertiesPanel = true;

}

onSelectionChange(args){
  if (args.state === "Changed" && args.newValue[0] instanceof Node &&  args.newValue[0].shape.type === 'UmlClassifier') {    
    this.selectedNode = args.newValue[0] as NodeModel;
  }
  else if (args.state === "Changed" && args.newValue[0] instanceof Connector) {
    // do some work
    }
}

saveDiagram(){
  
this.saveData = this.diagram.saveDiagram();
localStorage.setItem('fileName', this.saveData);
//console.log('saveDiagram -==== > ', saveData );
console.log('nodes -==== > ', this.diagram.nodes);
console.log('connectors  -==== > ', this.diagram.connectors);
  //console.log('connectors -==== > ', JSON.parse(saveData).connectors);
  //console.log('nodes -==== > ', JSON.parse(saveData).nodes);
}



//////////////////////////////


 nodeClasses: NodeModel[] = [
  {
    id: 'Patient',
    shape: {
      type: 'UmlClassifier',
      classShape: {
        name: 'Patient',
        attributes: [
          createProperty('accepted', 'Date'),        
        ],
        methods: [createMethods('getHistory', 'History')]
      },
      classifier: 'Class'
    } as UmlClassifierShapeModel,
    offsetX: 200,
    offsetY: 250
  },
  {
    id: 'Doctor',
    shape: {
      type: 'UmlClassifier',
      classShape: {
        name: 'Doctor',
        attributes: [
          createProperty('specialist', 'String[*]'),
        ]
      },
      classifier: 'Class'
    } as UmlClassifierShapeModel,
    offsetX: 240,
    offsetY: 545
  },
  {
    id: 'Person',
    shape: {
      type: 'UmlClassifier',
      classShape: {
        name: 'Person',
        attributes: [
          createProperty('name', 'Name'),
        ]
      },
      classifier: 'Class'
    } as UmlClassifierShapeModel,
    offsetX: 405,
    offsetY: 105
  },
  {
    id: 'Hospital',
    shape: {
      type: 'UmlClassifier',
      classShape: {
        name: 'Hospital',
        attributes: [
          createProperty('name', 'Name'),
          createProperty('address', 'Address'),
          createProperty('phone', 'Phone')
        ],
        methods: [createMethods('getDepartment', 'String')]
      },
      classifier: 'Class'
    } as UmlClassifierShapeModel,
    offsetX: 638,
    offsetY: 100
  },
  {
    id: 'Department',
    shape: {
      type: 'UmlClassifier',
      classShape: {
        name: 'Department',
        methods: [createMethods('getStaffCount', 'Int')]
      },
      classifier: 'Class'
    } as UmlClassifierShapeModel,
    offsetX: 638,
    offsetY: 280
  },
  {
    id: 'Staff',
    shape: {
      type: 'UmlClassifier',
      classShape: {
        name: 'Staff',
        attributes: [
          createProperty('education', 'string[*]'),
          createProperty('certification', 'string[*]'),
          createProperty('languages', 'string[*]')
        ],
        methods: [
          createMethods('isDoctor', 'bool'),
          createMethods('getHistory', 'bool')
        ]
      },
      classifier: 'Class'
    } as UmlClassifierShapeModel,
    offsetX: 635,
    offsetY: 455
  }
];

loadDiagram(){ 
    
    // this.diagram.loadDiagram(this.saveData);
 this.diagram.loadDiagram(JSON.stringify(getSavedDiagram()));
}   
  

/////////////////////////////





}
